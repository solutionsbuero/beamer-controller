import sys
import serial
import time

class ProjectorController:

	def __init__(self):
		self.port = "/dev/ttyS0"
		self.on_code = "\x02\x00\x00\x00\x00\x02"
		self.off_code = "\x02\x01\x00\x00\x00\x03"
		self.on_code_alt = [2,0,0,0,0,2]
		self.off_code_alt = [2,1,0,0,0,3]
		self.on_code_alt2 = [b'\x02', b'\x00', b'\x00', b'\x00', b'\x00', b'\x02']
		self.off_code_alt2 = [b'\x02', b'\x01', b'\x00', b'\x00', b'\x00', b'\x03']
		self.on_code_alt3 = [0x02, 0x00, 0x00, 0x00, 0x00, 0x02]
		self.off_code_alt3 = [0x02, 0x01, 0x00, 0x00, 0x00, 0x03]


	def encode_command(self, cmd_array):
		bin_cmd = bytearray(cmd_array)
		return bin_cmd

	def transmit_code(self, tx_code):
		port = serial.Serial(self.port, baudrate=38400, bytesize=8, parity=serial.PARITY_NONE, stopbits=1, timeout=5)
		port.write(tx_code.encode())
		received = port.read(8)
		print(received)  # newline is printed
		port.close

	def transmit_code_alt(self, tx_code):
		bin_code = bytearray(tx_code)
		port = serial.Serial(self.port, baudrate=38400, bytesize=8, parity=serial.PARITY_NONE, stopbits=1, timeout=5)
		port.write(bin_code[0:5])
		received = port.read(8)
		print(received)  # newline is printed
		port.close

	def transmit_code_alt2(self, tx_code):
		port = serial.Serial(self.port, baudrate=38400, bytesize=8, parity=serial.PARITY_NONE, stopbits=1, timeout=5)
		for i in tx_code:
			port.write(i)
		received = port.read(8)
		print(received)
		port.close()

	def transmit_code_alt3(self, tx_code):
		port = serial.Serial(self.port, baudrate=9600, bytesize=8, parity=serial.PARITY_NONE, stopbits=1, timeout=5)
		port.write(serial.to_bytes(tx_code))
		received = port.read(8)
		print(received)  # newline is printed
		port.close

if __name__ == '__main__':
	pc = ProjectorController()
	while True:
		'''
		pc.transmit_code(pc.off_code)
		print("just transmitted the ON code")
		time.sleep(180)
		pc.transmit_code(pc.on_code)
		print("just transmitted the OFF code")
		time.sleep(180)
		
		pc.transmit_code_alt(pc.on_code_alt)
		print("just transmitted the ON code")
		time.sleep(60)
		pc.transmit_code_alt(pc.off_code_alt)
		print("just transmitted the ON code")
		time.sleep(60)
		'''

		port = serial.Serial("/dev/ttyS0", baudrate=9600, bytesize=8, parity=serial.PARITY_NONE, stopbits=1, timeout=5)
		#port.write(serial.to_bytes([0x02, 0x01, 0x00, 0x00, 0x00, 0x03]))
		port.write(serial.to_bytes([0x02, 0x03, 0x00, 0x00, 0x02, 0x01, 0xa1, 0xa9]))
		received = port.read(8)
		print(received)  # newline is printed
		port.close

		print("Sent OFF command")
		time.sleep(60)

		port = serial.Serial("/dev/ttyS0", baudrate=9600, bytesize=8, parity=serial.PARITY_NONE, stopbits=1, timeout=5)
		#port.write(serial.to_bytes([0x02, 0x00, 0x00, 0x00, 0x00, 0x02]))
		port.write(serial.to_bytes([0x02, 0x03, 0x00, 0x00, 0x02, 0x01, 0x01, 0x09]))
		received = port.read(8)
		print(received)  # newline is printed
		port.close

		print("Sent ON command")
		time.sleep(60)